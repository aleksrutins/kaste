# kaste

A simple library for storing data in GNOME applications.

See the documentation [here](https://aleksrutins.pages.gitlab.gnome.org/kaste/).

## "Kaste"?

"Kaste" means "box" in Latvian. Calling it "libbox" seemed a bit boring and potentially confusing, so "kaste" it is.

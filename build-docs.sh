#!/bin/sh
rm -rf public

version=0.1

valadoc -o valadoc-public --package-name=kaste-$version --vapidir=src --vapidir=builddir/src --pkg=gio-2.0 --pkg=glib-2.0 --pkg=gobject-2.0 --pkg=json-glib-1.0 --gir=Kaste-$version.gir --verbose src/bucket.vala
gi-docgen generate -C kaste.toml Kaste-$version.gir
mv Kaste-$version public

namespace Kaste {
    public const int BUCKET_LOCKED = 1;
    class BucketInfo : Object {
        public string owner;
        public bool shared;

        public BucketInfo(string owner, bool shared) {
            this.owner = owner;
            this.shared = shared;
        }

        public bool can_access(Application app) {
            return shared || GLib.Environment.get_prgname() == owner;
        }
    }

    /**
     * An abstraction over a directory in the application's XDG data directory.
     *
     * Items in a bucket are serialized and deserialized using JSON-GLib, so serialization behavior can be customized by implementing {@link Json.Serializable}.
     */
    public class Bucket : Object {
        public string rdns { get; construct; }
        public bool shared { get; construct; }
        public Application app {private get; construct;}
        private bool locked = false;

        /**
         * The path to the bucket's storage.
         */
        public string path {
            owned get {
                var rdns_path = this.rdns.replace(".", "/");
                var path = new PathBuf.from_path(Environment.get_user_data_dir());
                path.push("bucket");
                path.push(rdns_path);
                return path.to_path();
            }
        }

        /**
         * Creates a new bucket.
         *
         * @param rdns The reverse domain name identifier of the bucket, usually starting with your app's app ID.
         * @param shared Whether this bucket is public or not. Public buckets are accessible to all apps with filesystem access to them, whereas attempting to access another app's private bucket will result in an error.
         */
        public Bucket(Application app, string rdns, bool shared) {
            Object(app: app, rdns: rdns, shared: shared);
        }

        construct {
            var path = this.path;
            var file = File.new_for_path(path);

            try {
                file.make_directory_with_parents();
                write("bucket-info", new BucketInfo(app.get_application_id(), shared));
            } catch(Error e) {
                if(e.code == IOError.EXISTS) {
                    assert(file.query_file_type(NOFOLLOW_SYMLINKS) == DIRECTORY);
                    var info = (BucketInfo)read(typeof(BucketInfo), "bucket-info");
                    locked = !info.can_access(app);
                }
            }
        }

        /**
         * Returns the path at which a resource would be located, whether or not that resource exists.
         * @param resource The name of a resource
         */
        public string get_resource_path(string resource) {
            var path = new PathBuf.from_path(this.path);
            path.push(resource);
            return path.to_path();
        }

        /**
         * Reads and deserializes a resource, returning the deserialized object or `null` on failure.
         *
         * @param t the type to deserialize to
         * @param resource the name of a resource
         */
        public Object? read(Type t, string resource) throws Error {
            if(locked) throw new Error(Quark.from_string("kaste-bucket-locked"), BUCKET_LOCKED, @"Access denied for bucket $rdns");
            try {
                string contents;
                var path = get_resource_path(resource);
                FileUtils.get_contents(path, out contents);
                return Json.gobject_from_data(t, contents);
            } catch(Error e) {
                return null;
            }
        }

        /**
         * Writes an object to a resource.
         * Returns the number of bytes written, or 0 on failure.
         *
         * @param resource the name of a resource
         * @param obj the object to write
         */
        public size_t write(string resource, Object obj) throws Error {
            if(locked) throw new Error(Quark.from_string("kaste-bucket-locked"), BUCKET_LOCKED, @"Access denied for bucket $rdns");
            try {
                size_t length;
                var data = Json.gobject_to_data(obj, out length);
                var file = File.new_for_path(get_resource_path(resource));
                file.replace_contents(data.data, null, false, NONE, null);
                return length;
            } catch(Error e) {
                return 0;
            }
        }

        /**
         * Returns a {@link GLib.FileEnumerator} for the bucket's directory.
         */
        public FileEnumerator list_contents() throws Error {
            if(locked) throw new Error(Quark.from_string("kaste-bucket-locked"), BUCKET_LOCKED, @"Access denied for bucket $rdns");
            return File.new_for_path(this.path).enumerate_children("standard::*", NOFOLLOW_SYMLINKS);
        }
    }
}
